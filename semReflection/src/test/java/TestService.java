import org.junit.Test;

import java.io.Serializable;
import java.util.Arrays;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static org.junit.Assert.*;

public class TestService {
    @Test
    public void testGetInfo() throws IllegalAccessException {
        Base base0 = new Base("Описание");
        Base base1 = new Base("Запасное описание");
        Base base2 = new Derived("Другое описание", "Информация");
        Derived derived0 = new Derived("Дополнительное описание", "Секретная информация");
        Derived derived1 = new Derived("Совокупность описаний", "Супер информация");
        Derived derived2 = new Derived("Крайнее описание", "Супер секретная информация");

        assertEquals(Arrays.asList("Описание", "Запасное описание", "Другое описание, Информация"),
                Service.getInfo(Arrays.asList(base0, base1, base2)));
        assertEquals(Arrays.asList("Дополнительное описание, Секретная информация", "Совокупность описаний, Супер информация",
                "Крайнее описание, Супер секретная информация"), Service.getInfo(Arrays.<Base>asList(derived0, derived1, derived2)));
    }


    @Test
    public void testIsFunctionalInterface() {
        assertFalse(Service.isFunctionalInterface(Base.class));
        assertFalse(Service.isFunctionalInterface(Derived.class));
        assertFalse(Service.isFunctionalInterface(Serializable.class));
        assertTrue(Service.isFunctionalInterface(Supplier.class));
        assertTrue(Service.isFunctionalInterface(Consumer.class));
    }


    @Test
    public void testIsImplementsSerializable() {
        assertTrue(Service.isImplementsSerializable(new Base("Новое описание")));
        assertFalse(Service.isImplementsSerializable(new Derived("Точное описание","Сессия")));
        assertTrue(Service.isImplementsSerializable(123));
        assertFalse(Service.isImplementsSerializable(new Object()));
    }


    @Test
    public void testIsSerializable() {
        assertTrue(Service.isSerializable(new Base("Новейшее описание")));
        assertTrue(Service.isSerializable(new Derived("Информативное описание","Сдача сессии")));
        assertTrue(Service.isSerializable(123));
        assertFalse(Service.isSerializable(new Object()));
    }


    @Test
    public void testGetStaticMethods() {
        assertEquals(Arrays.asList("Service", "java.util.Arrays"),
                Service.getStaticMethods(Arrays.asList(Base.class, Service.class, Arrays.class,
                        Consumer.class, Supplier.class, TestService.class)));
    }
}
