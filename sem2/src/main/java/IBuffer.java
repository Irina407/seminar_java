//интерфейс IBuffer
//буфер может иметь ограничение на размер
public interface IBuffer {
    void addElem(Task elem); //добавить элемент в буфер
    Task getElem(); //взять элемент из буфера (метод без параметров, извлекается какой-то элемент)
    int quantity(); //кол-во элементов в буфере
    void clear(); //очистка буфера
    boolean isEmpty(); //проверка на пустоту
}
