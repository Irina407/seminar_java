//интерфейс ITaskProcessor с методом process, который получает на вход задачу и возвращает целое число типа Integer.
public interface ITaskProcessor {
    Integer process();
}
