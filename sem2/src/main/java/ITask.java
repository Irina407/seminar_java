//интерфейс ITask с методом getData, который не получает входных
//параметров и возвращает массив целых чисел.

public interface ITask {
    int[] getData();
}
