import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

public class TestSimpleTaskProcessor {
    @Test
    public void testSimpleTaskProcessor() {
        assertDoesNotThrow(() -> new SimpleTaskProcessor(new SimpleBuffer()));
        try{
            SimpleTaskProcessor processor = new SimpleTaskProcessor(null);
        }
        catch (NullPointerException e){
            assertEquals("Null buffer", e.getMessage());
        }
    }

    @Test
    public void testProcessEmptyBuffer() {
        SimpleTaskProcessor processor1 = new SimpleTaskProcessor(new SimpleBuffer());

        assertNull(processor1.process());
    }

    @Test
    public void testProcessOneTask() {
        SimpleBuffer buffer = new SimpleBuffer();
        SimpleTaskProcessor processor = new SimpleTaskProcessor(buffer);

        buffer.addElem(new Task(4, -7, 25));

        assertEquals(Integer.valueOf(22), processor.process());
        assertNull(processor.process());
    }

    @Test
    public void testProcessTwoTasks() {
        SimpleBuffer buffer = new SimpleBuffer();
        SimpleTaskProcessor processor = new SimpleTaskProcessor(buffer);

        buffer.addElem(new Task(9, 6, -19));
        new SimpleTaskGenerator(buffer, 1, 6).generate();

        assertEquals(Integer.valueOf(-4), processor.process());
        assertEquals(Integer.valueOf(21), processor.process());
        assertNull(processor.process());
    }
}
