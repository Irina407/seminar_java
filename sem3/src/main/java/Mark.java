//тип как enum или перечисление. Перечисления представляют набор логически связанных констант.
// Объявление перечисления происходит с помощью оператора enum, после которого идет название перечисления.
public enum Mark {
    SET_OFF_FAIL(0, "Не зачтено"),
    SET_OFF_PASS(1, "Зачтено"),
    FAIL(2, "Неудовлетворительно"),
    PASS(3, "Удовлетворительно"),
    PASS_GOOD(4, "Хорошо"),
    PASS_EXCELLENT(5, "Отлично");


    private int value;
    private String name;


    Mark(int value, String name) {
        this.value = value;
        this.name = name;
    }


    public int getValue() {
        return value;
    }


    public String getName() {
        return name;
    }
}
