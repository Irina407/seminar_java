import java.util.List;
import java.util.Objects;

//Справка состоит из шапки и табличной части. Шапка документа содержит:
public class Reference {
    private String student; //ФИО студента
    private String university; //название вуза
    private String faculty; //факультет
    private String speciality; //специальность
    //период обучения
    private String beginDate; //дата начала
    private String endDate; //дата окончания
    private List<TableString> table;

    public Reference(String student, String university, String faculty, String speciality,
                       String beginDate, String endDate, List<TableString> table) {
        this.student = student;
        this.university = university;
        this.faculty = faculty;
        this.speciality = speciality;
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.table = table;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public List<TableString> getTable() {
        return table;
    }

    public void setTable(List<TableString> table) {
        this.table = table;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reference that = (Reference) o;
        return Objects.equals(student, that.student) &&
                Objects.equals(university, that.university) &&
                Objects.equals(faculty, that.faculty) &&
                Objects.equals(speciality, that.speciality) &&
                Objects.equals(beginDate, that.beginDate) &&
                Objects.equals(endDate, that.endDate) &&
                Objects.equals(table, that.table);
    }

    @Override
    public int hashCode() {
        return Objects.hash(student, university, faculty, speciality, beginDate, endDate, table);
    }
}
