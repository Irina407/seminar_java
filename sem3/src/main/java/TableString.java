import java.util.Objects;

//Табличная часть — это набор строк, каждая из которых содержит:
public class TableString {
    private String discipline; //название дисциплины
    private int laborInput; //трудоемкость дисциплины в часах
    private Mark mark; //оценку

    public TableString(String discipline, int laborInput, Mark mark) {
        setDiscipline(discipline);
        setLaborInput(laborInput);
        setMark(mark);
    }

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    public int getLaborInput() {
        return laborInput;
    }

    public void setLaborInput(int laborInput) { this.laborInput = laborInput; }

    public Mark getMark() {
        return mark;
    }

    public void setMark(Mark mark) {
        this.mark = mark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TableString that = (TableString) o;
        return laborInput == that.laborInput &&
                Objects.equals(discipline, that.discipline) &&
                mark == that.mark;
    }

    @Override
    public int hashCode() {
        return Objects.hash(discipline, laborInput, mark);
    }
}
