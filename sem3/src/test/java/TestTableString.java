import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TestTableString {

    @Test
    public void testTableStringExam() {
        TableString string = new TableString("Функциональный анализ", 128, Mark.FAIL);

        assertEquals("Функциональный анализ", string.getDiscipline());
        assertEquals(128, string.getLaborInput());
        assertEquals(Mark.FAIL, string.getMark());
    }


    @Test
    public void testTableStringSetOff() {
        TableString string = new TableString("С++", 144, Mark.SET_OFF_PASS);
        TableString string2 = new TableString("Физкультура", 196, Mark.SET_OFF_FAIL);

        assertEquals("С++", string.getDiscipline());
        assertEquals(144, string.getLaborInput());
        assertEquals("Физкультура", string2.getDiscipline());
        assertEquals(196, string2.getLaborInput());
        assertEquals(Mark.SET_OFF_PASS, string.getMark());
        assertEquals(Mark.SET_OFF_FAIL, string2.getMark());
    }

    @Test
    public void testEquals() {
        TableString string1 = new TableString("ЯМПЫ", 258, Mark.PASS);
        TableString string2 = new TableString("ЯМПЫ", 258, Mark.PASS);
        TableString string3 = new TableString("Программирование на C++", 128, Mark.PASS);
        TableString string4 = new TableString("ЯМПЫ", 200, Mark.PASS);

        assertEquals(string1, string1);
        assertEquals(string1, string2);
        assertEquals(string2, string1);
        assertNotEquals(string1, "");
        assertNotEquals(string1, string3);
        assertNotEquals(string1, string4);
    }
}